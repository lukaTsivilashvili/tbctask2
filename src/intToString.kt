fun intToString(num: Int): String {

    val numOnes = listOf<String>(
        "", "ერთი", "ორი", "სამი", "ოთხი", "ხუთი", "ექვსი", "შვიდი", "რვა", "ცხრა", "ათი",
        "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი", "თხუტმეტი", "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი"
    )
    val numTensGeo = listOf<String>("", "", "ოცდა", "ოცდა", "ორმოცდა", "ორმოცდა", "სამოცდა", "სამოცდა", "ოთხმოცდა", "ოთხმოცდა")
    val numTens = listOf<String>("", "ათი", "ოცი", "ოცდაათი", "ორმოცი", "ორმოცდაათი", "სამოცი", "სამოცდაათი", "ოთხმოცი" ,"ოთხმოცდაათი")
    val numHundredsGeo = listOf<String>("", "ას", "ორას", "სამას", "ოთხას", "ხუთას", "ექვსას", "შვიდას", "რვაას", "ცხრაას")
    val numHundreds = listOf<String>("", "ასი", "ორასი", "სამასი", "ოთხასი", "ხუთასი", "ექვსასი", "შვიდასი", "რვაასი", "ცხრაასი")

    var numString = num.toString()
    var result = ""

    if (num <= 19) {
        result = "შეყვანილი რიცხვია: " + numOnes[num]
    }

    if (num in 20..99) {

        if (num % 10 == 0){
            result = "შეყვანილი რიცხვია: " + numTens[num/10]
        }else{
            result = "შეყვანილი რიცხვია: " + numTensGeo[num / 10] + numOnes[num % 20]
        }
    }

    if (num == 0){
        return "შეყვანილი რიცხვია: 0"
    }

    if (num in 99..999) {
        val middleNum = numString[1].toString()
        val lastNum = num % 20

        if (num % 100 == 0){
            result = "შეყვანილი რიცხვია: " + numHundreds[num/100]
        }else if (num % 10 == 0){
            result = "შეყვანილი რიცხვია: " + numHundredsGeo[num/100] + " " + numTens[middleNum.toInt()]
        }else{
            result = "შეყვანილი რიცხვია: " + numHundredsGeo[num / 100] + " " + numTensGeo[middleNum.toInt()] + numOnes[lastNum]
        }
    }

    if (num == 1000){
        result =  "შეყვანილი რიცხვია: ათასი"
    }else if (num > 1000){
        result = "მოქმედება შეუძლებელია :("
    }

    return result
}

fun main() {
    for (i in 1..1000){
        println(intToString(i))
    }
}